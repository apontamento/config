from django.urls import path

from config.apontamento.views import login

urlpatterns = [
    path('', login)
]